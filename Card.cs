using System;

namespace hilowsolitaire
{
    public class Card
    {
        private int Value;
        // 1 = Ace, 11 = Jack, 12 = Queen, 13 = King
        private string Face;

        public Card()
        {
            Value = 0;
            Face = "uninitialized";
        }
        public Card(int value, string face)
        {
            Value = value; 
            Face = face;
        }
        public Card(Card old)
        {
            Value = old.Value;
            Face = old.Face;
        }
        public void set(int value, string face)
        {
            // Setter function makes code a bit more readable outside Card.cs
            Value = value;
            Face = face;
        }
        public int getValue()
        {
            return Value;
        }
        public void printCard()
        {
            if (Value > 1 && Value < 11)
            {
                Console.WriteLine(Value + " of " + Face);
            }
            else if (Value == 1)
            {
                Console.WriteLine("Ace of " + Face);
            }
            else if (Value == 11)
            {
                Console.WriteLine("Jack of " + Face);
            }
            else if (Value == 12)
            {
                Console.WriteLine("Queen of " + Face);
            }
            else if (Value == 13)
            {
                Console.WriteLine("King of " + Face);
            }
            else
            {
                Console.WriteLine(Value + " of " + Face);
            }
        } 
    }
}