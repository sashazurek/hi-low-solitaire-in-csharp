﻿using System;
using System.Collections.Generic;

namespace hilowsolitaire
{
    class Game
    {
        // Seeds random number generator for initalizeDeck()
        static Random rng = new Random();
        public static bool verifyGuess(string input, Card flipped, Stack<Card> pile)
        {
            if (input == "h")
            {
                if (flipped.getValue() >= pile.Peek().getValue())
                    return false;
            }
            else
            {
                if (flipped.getValue() <= pile.Peek().getValue())
                    return false;
            }

            return true;
        }
        // Deals deck into the 4 piles
        public static string inputQuery()
        {
            string input;
            bool valid = false;

            do
            {
                Console.Write("(H)igher, (l)ower, (s)hift, or (q)uit?: ");
                input = Console.ReadLine();
                Console.WriteLine(input);
                switch (input)
                {
                    case "h":
                    case "l":
                    case "s":
                    case "q":
                        valid = true;
                        break;
                    case "f":
                        Console.WriteLine("Wow, you found the hidden function");
                        valid = true;
                        break;
                    default:
                        Console.WriteLine("Invalid input.");
                        break;
                }
            } while (!valid);
            return input;
        }
        public static void dealDeck(Card[] deck,Stack<Card>[] thePiles)
        {
            int i;

            for (i=0; i < 13; i++)
                thePiles[0].Push(deck[i]);
            for (i=13; i < 26; i++)
                thePiles[1].Push(deck[i]);
            for (i=26; i < 39; i++)
                thePiles[2].Push(deck[i]);
            for (i=39; i < 52; i++)
                thePiles[3].Push(deck[i]);

        }
        // Initializes and shuffles the deck
        public static void initializeDeck(Card[] deck)
        {
            int i, k;
            Card temp;

            for (i=0; i < 13; i++)
                deck[i] = new Card(i+1, "Spades");
            for (i=13; i < 26; i++)
                deck[i] = new Card(i-12,"Clubs");
            for (i=26; i < 39; i++)
                deck[i] = new Card(i-25,"Hearts");
            for (i=39; i < 52; i++)
                deck[i] = new Card(i-38,"Diamonds");

            // Fisher-Yates shuffle
            i = 52;
            while (i > 1)
            {
                k = rng.Next(i--);
                temp = deck[i];
                deck[i] = deck[k];
                deck[k] = temp;
            }

        }
        public static void printUserStatus(int cashOnHand)
        {
            Console.WriteLine("Money: $" + cashOnHand + "\n");
        }
        // Contains game logic for Hi-Low Solitaire and actions
        public static int theGame(int cashOnHand)
        {
            int condition = 0;
            int pile = 0;
            int i;
            string userInput;
            Stack<Card>[] thePiles = new Stack<Card>[4];
            Card hold = new Card();
            Card[] deck = new Card[52];

            // Initialize the stacks in stack array thePiles
            for (i = 0; i < 4; i++)
                thePiles[i] = new Stack<Card>();
            
            printUserStatus(cashOnHand);
            initializeDeck(deck);
            dealDeck(deck, thePiles);

            // Allow user to continue guessing until either:
                // User guesses wrong
                // User wants to quit
            // Both result in the end of the round
            do
            {
                hold = thePiles[pile].Pop();


                if (!thePiles[pile].TryPeek(out Card result))
                {
                    Console.WriteLine("\n\nWow! You guessed a whole pile correctly!\n");
                    Console.WriteLine("+$500 to your wallet!\n\n\n");
                    cashOnHand += 500;
                    printUserStatus(cashOnHand);
                    pile++;
                    hold = thePiles[pile].Pop();
                }
                Console.WriteLine("Current Pile: " + (pile+1));
                Console.Write("Card: ");
                hold.printCard();
                // Padding 
                Console.WriteLine();

                userInput = inputQuery();
                
                switch (userInput)
                {
                    case "h":
                    case "l":
                        // guess condition
                        if (verifyGuess(userInput, hold, thePiles[pile]))
                        {
                            Console.WriteLine("Good Guess! +$10\n");
                            cashOnHand += 10;
                        }
                        else
                        {
                            Console.WriteLine("Bad Guess! You lose :(\n");
                            condition = 1;
                        }
                        break;
                    case "f":
                        Console.WriteLine("Passing card...");
                        break;
                    case "s":
                        // shift condition
                        if (pile != 3)
                            pile++;
                        else
                            condition = 2;
                        break;
                    case "q":
                        // quit condition
                        condition = 1;
                        break;
                    default:
                        Console.WriteLine("This shouldn't happen.");
                        break;
                }
                if (condition == 0)                
                    printUserStatus(cashOnHand);

            } while (condition == 0);

            return condition;
        }
        public static void Main()
        {
            int cashOnHand = 500; // 500 is starting amount
            int status = 0; // Used to determine if program ends

            do
            {
                if (cashOnHand >= 150) //150 is game price
                {
                    Console.WriteLine("Starting Game. -$150");
                    cashOnHand -= 150;
                    status = theGame(cashOnHand);
                }
                else
                {
                    Console.WriteLine("Sorry, you ran out of money!");
                    status = 1;
                }
                if (status == 1)
                    Console.WriteLine("Goodbye! Thanks for playing.");
            } while (status != 1);
        }
    }
}
